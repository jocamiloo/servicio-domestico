import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { DashboardComponent } from './servicio-domestico/dashboard/dashboard/dashboard.component';
import { ContenidoLiquidacionComponent } from './servicio-domestico/contenido-liquidacion/contenido-liquidacion.component';

const routes: Routes = [{
  path: 'dashboard',
  component: DashboardComponent,
  pathMatch: 'full',
},
{
  path: 'servicio-domestico',
  component: ContenidoLiquidacionComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],

})
export class AppRoutingModule { }


