import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContenidoLiquidacionComponent } from './servicio-domestico/contenido-liquidacion/contenido-liquidacion.component';
import { CalculoLiquidacionComponent } from './servicio-domestico/calculo-liquidacion/calculo-liquidacion.component';
import { HttpClientModule } from "@angular/common/http";
import { ServicioDomesticoRoter } from './servicio-domestico/servicio-domestico.router';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './shared/_module/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './servicio-domestico/dashboard/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    ContenidoLiquidacionComponent,
    CalculoLiquidacionComponent,
    DashboardComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    SharedModule.forRoot(),
    ServicioDomesticoRoter,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
