import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-calculo-liquidacion',
  templateUrl: './calculo-liquidacion.component.html',
  styleUrls: ['./calculo-liquidacion.component.css']
})
export class CalculoLiquidacionComponent implements OnInit {

  @Input() public salarioBaseMensual: number = 828116;
  @Input() public diasALiquidar: number = 0;
  @Input() public baseLiquidacion: number = 0;
  @Input() public prima: number = 0;
  @Input() public cesantias: number = 0;
  @Input() public interesCesantias: number = 0;
  @Input() public diasVacaciones: number = 0;
  @Input() public diasVacacionesTomados: number = 0;
  @Input() public vacaciones: number = 0;
  @Input() public indemnizacion: number = 0;
  @Input() public auxilioTrasporte: number = 0;
  @Input() public totalPagar: number = 0;
  
  constructor() { }

  ngOnInit() {
  }

}
