import { Component, OnInit } from '@angular/core';
import { ContenidoLiquidacionValidacion } from './contenido-liquidacion.validate';
import * as moment from 'moment';
import { DomesticoService } from 'src/app/shared/_services/domestico.services';
import { SERVICE_CONSTANTS } from 'src/app/shared/_util/services-util';
@Component({
  selector: 'app-contenido-liquidacion',
  templateUrl: './contenido-liquidacion.component.html',
  styleUrls: ['./contenido-liquidacion.component.css'],
})
export class ContenidoLiquidacionComponent extends ContenidoLiquidacionValidacion implements OnInit {

  constructor(private service: DomesticoService) {
    super();
  }

  ngOnInit() {
    this.getGroupValidator();
    this.rsForm.controls['termino'].setValue('N');
    /** tipo vinculación tiempo completo */
    this.rsForm.controls['tipoVinculacion'].setValue('C');
    /** Salario basico mensual */
    this.rsForm.controls['salarioBaseMensual'].setValue(828116);
    /** Auxilio transporte mensual */
    this.rsForm.controls['auxilioTrasporte'].setValue(97032);
    /** base de la liquidación */
    this.rsForm.controls['baseLiquidacion'].setValue(828116 + 97032);
    /** cesantías */
    this.rsForm.controls['cesantias'].setValue(0);
    /** intereses cesantías */
    this.rsForm.controls['interesCesantias'].setValue(0);
    /** prima de servicios*/
    this.rsForm.controls['prima'].setValue(0);
    /** Vacaciones */
    this.rsForm.controls['vacaciones'].setValue(0);
    this.rsForm.controls['diasVacaciones'].setValue(0);
    this.rsForm.controls['diasVacacionesTomados'].setValue(0);
    this.rsForm.controls['vacionesPagas'].setValue('N');
  }

  calcular() {
    if (this.rsForm.valid) {
      /** Calculo cesantias 
      var cesantias = (Number(this.rsForm.value.baseLiquidacion) * Number(this.rsForm.value.diasALiquidar)) / 360;
        this.rsForm.controls['cesantias'].setValue(Math.round(cesantias));*/
      /** Intereses cesantías
      var interesCesantias = (cesantias * Number(this.rsForm.value.diasALiquidar) * 0.12) / 360;
      this.rsForm.controls['interesCesantias'].setValue(Math.round(interesCesantias)); */
      /** Prima de servicios var primaServicio = (Number(this.rsForm.value.baseLiquidacion) * Number(this.rsForm.value.diasALiquidar)) / 360;
      this.rsForm.controls['prima'].setValue(Math.round(primaServicio)); */

      /** Vacaciones
      var vacaciones = (Number(this.rsForm.value.salarioBaseMensual) * Number(this.rsForm.value.diasALiquidar)) / 720;
      var diasVacaciones = (Number(this.rsForm.value.diasALiquidar) * 15) / 360;
      var descuentoVacaciones = (Number(this.rsForm.value.salarioBaseMensual) / 30) * Number(this.rsForm.value.diasVacacionesTomados);
      this.rsForm.controls['vacaciones'].setValue(Math.round(vacaciones - descuentoVacaciones));
      this.rsForm.controls['diasVacaciones'].setValue(diasVacaciones - Number(this.rsForm.value.diasVacacionesTomados));
      */
      this.calculo();
    }
  }

  calculo() {
    /** Calculo cesantias  */
    var cesantias = { baseLiquidacion: Number(this.rsForm.value.baseLiquidacion), diasALiquidar: Number(this.rsForm.value.diasALiquidar) };
    this.service.post(SERVICE_CONSTANTS.CALCULO_CESANTIAS, cesantias).subscribe(
      (cesantias: any) => {
        this.rsForm.controls['cesantias'].setValue(cesantias);
        /** Intereses cesantías */
        var interesCesantias = { cesantias: cesantias, diasALiquidar: Number(this.rsForm.value.diasALiquidar) };
        /** Prima de servicios */
        this.service.post(SERVICE_CONSTANTS.CALCULO_INTERES_CESANTIAS, interesCesantias).subscribe(
          (interesCesantia: any) => {
            this.rsForm.controls['interesCesantias'].setValue(Math.round(interesCesantia));
            var prima = { baseLiquidacion: Number(this.rsForm.value.baseLiquidacion), diasALiquidar: Number(this.rsForm.value.diasALiquidar) };
            this.service.post(SERVICE_CONSTANTS.CALCULO_PRIMA_SERVICIO, prima).subscribe(
              (primaServicio: any) => {
                this.rsForm.controls['prima'].setValue(Math.round(primaServicio));
                /** Vacaciones */
                var vacaciones = { salarioBaseMensual: this.rsForm.value.salarioBaseMensual, diasALiquidar: this.rsForm.value.diasALiquidar };
                this.service.post(SERVICE_CONSTANTS.CALCULO_VACACIONES, vacaciones).subscribe(
                  (vacionesValor: any) => {
                    var diasVacaciones = (Number(this.rsForm.value.diasALiquidar) * 15) / 360;
                    var descuentoVacaciones = (Number(this.rsForm.value.salarioBaseMensual) / 30) * Number(this.rsForm.value.diasVacacionesTomados);
                    this.rsForm.controls['vacaciones'].setValue(Math.round(vacionesValor - descuentoVacaciones));
                    this.rsForm.controls['diasVacaciones'].setValue(diasVacaciones - Number(this.rsForm.value.diasVacacionesTomados));

                    /** Total a pagar */
                    var total = cesantias + interesCesantia + primaServicio + Number(vacionesValor);
                    this.rsForm.controls['totalPagar'].setValue(total);

                  },
                  (error: any) => {

                  }
                );
              },
              (error: any) => {

              }
            );
          },
          (error: any) => {

          }
        );
      },
      (error: any) => {

      }
    );
  }

  totalDiasLaborados(e, msj: string) {
    if (this.rsForm.value.fechaInicial != undefined && this.rsForm.value.fechaInicial != null &&
      this.rsForm.value.fechaFin != undefined && this.rsForm.value.fechaFin != null) {
      if (this.rsForm.value.fechaFin < this.rsForm.value.fechaInicial) {
        e.setErrors({ 'incorrect': true, message: msj });
      } else {
        var fechaInicial = moment(this.rsForm.value.fechaInicial);
        var fechaFin = moment(this.rsForm.value.fechaFin);
        console.log(fechaFin.diff(fechaInicial, 'days') + 1, ' dias de diferencia');
        this.rsForm.controls['diasALiquidar'].setValue(fechaFin.diff(fechaInicial, 'days') + 1);
      }

    }
  }

  salarioBaseMensual() {
    if (this.rsForm.value.tipoVinculacion == 'D') {
      if (this.rsForm.value.diasTrabajoSemanal != undefined && this.rsForm.value.diasTrabajoSemanal != null &&
        this.rsForm.value.valorDia != undefined && this.rsForm.value.valorDia != null) {
        /** salario base mensual calculado por la cantidad de dias laborados semanalmente. */
        this.rsForm.controls['salarioBaseMensual'].setValue((this.rsForm.value.valorDia * this.rsForm.value.diasTrabajoSemanal) * 4.33);
        /** Auxilio de transporte mensual, calculado por la cantidad de dias laborados semanalmente. */
        this.rsForm.controls['auxilioTrasporte'].setValue(Math.round(3234 * (this.rsForm.value.diasTrabajoSemanal * 4.33)));
        /** base de la liquidación */
        this.rsForm.controls['baseLiquidacion'].setValue(Number(this.rsForm.value.salarioBaseMensual) + Number(this.rsForm.value.auxilioTrasporte));
        this.calcular();
      }
    } else if (this.rsForm.value.tipoVinculacion == 'C') {
      /** salario base mensual. */
      if (this.rsForm.value.valorMensual != undefined && this.rsForm.value.valorMensual != null && this.rsForm.value.valorMensual > 0) {
        this.rsForm.controls['salarioBaseMensual'].setValue(this.rsForm.value.valorMensual);
        /** Auxilio de transporte mensual */
        this.rsForm.controls['auxilioTrasporte'].setValue(97032);
        /** base de la liquidación */
        this.rsForm.controls['baseLiquidacion'].setValue(Number(this.rsForm.value.valorMensual) + 97032);
        this.calcular();
      } else {
        this.rsForm.controls['salarioBaseMensual'].setValue(828116);
        /** Auxilio transporte mensual */
        this.rsForm.controls['auxilioTrasporte'].setValue(97032);
        /** base de la liquidación */
        this.rsForm.controls['baseLiquidacion'].setValue(828116 + 97032);
      }


    }

  }

}
