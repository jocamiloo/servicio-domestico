import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ValidatorUtils } from 'src/app/shared/_util/validator-util';


export class ContenidoLiquidacionValidacion extends ValidatorUtils {

    /** Formularios */
    public rsForm: FormGroup;

    /** The FormBuilder */
    public formBuilder: FormBuilder;

    constructor(){
        super();
        this.formBuilder = new FormBuilder();
    }

    public getGroupValidator() {
        this.rsForm = this.formBuilder.group({
            codigo: [],
            termino: this.getValidationRequired(),
            motivo: [],
            fechaInicial: this.getValidationRequired(),
            fechaFin: this.getValidationRequired(), 
            tipoVinculacion: [],  
            diasALiquidar: [],
            diasTrabajoSemanal: [],
            valorDia: [],
            valorMensual: [],
            salarioBaseMensual:  [],  
            baseLiquidacion: [],  
            prima: [],  
            cesantias:  [],  
            interesCesantias:  [],  
            vacionesPagas: [],
            diasVacaciones: [],
            diasVacacionesTomados: [],
            vacaciones:  [],  
            indemnizacion:  [],  
            auxilioTrasporte:  [],  
            totalPagar: []
        });
    }
    
    private getValidationMonedaAsociada(): { [key: string]: any } {
        return [null, Validators.compose([
            ValidatorUtils.required("Campo requerido"),
            ValidatorUtils.maxLength(10, 'Máximo 10 caracteres')]
        )];
    }

        
}
