import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ContenidoLiquidacionComponent } from './contenido-liquidacion/contenido-liquidacion.component';

const SERVICIO_ROUTES: Routes = [
    { path: 'dashboard', component: ContenidoLiquidacionComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
    { path: '**', component: ContenidoLiquidacionComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(SERVICIO_ROUTES, { useHash: true })],
    providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}],
  })
export class ServicioDomesticoRoter { }

