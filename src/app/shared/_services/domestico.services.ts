import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders,  HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// httpOptions for set header options
const httpOptions = {
  headers: new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }),
};

const path = environment.DOMESTICO_SERVICE;

@Injectable({
  providedIn: 'root'
})

export class DomesticoService {

  constructor(private http: HttpClient) {}

  get(endpoint) {
    return this.http.get(path + endpoint).pipe(
      catchError(this.handleError)
    );
  }

  post(endpoint, element) {
    return this.http.post(path + endpoint, element, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  putWithOutParam(endpoint, element) {
    return this.http.put(path + endpoint, element, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  put(endpoint, element) {
    return this.http.put(path + endpoint + '/' + element.codigo, element, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
  
  delete(endpoint, id) {
    return this.http.delete(path + endpoint + '/' + id, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(error.error);
  }

}
