export const CONSTANTES_UTIL = {
    NAME_TOKEN: 'access_token',
    CODE: 'code',
    TOKEN: 'token',
    REDIRECT_URI: '&redirect_uri=',
    CLIENT_SECRET: 'secret',
    GRAND_TYPE_P: 'password',
    GRAND_TYPE_C: 'authorization_code',
    ELEMENT_MODULO: 'modulos',
    CURRENT_USER: 'currentUser'
};
