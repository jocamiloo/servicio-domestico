export const SERVICE_CONSTANTS = {
    CALCULO_CESANTIAS: 'servicio-domestico-backend/v1/calculo/cesantias',
    CALCULO_INTERES_CESANTIAS: 'servicio-domestico-backend/v1/calculo/interesesCesantias',
    CALCULO_PRIMA_SERVICIO: 'servicio-domestico-backend/v1/calculo/primaServicio',
    CALCULO_VACACIONES: 'servicio-domestico-backend/v1/calculo/vacaciones'
};
