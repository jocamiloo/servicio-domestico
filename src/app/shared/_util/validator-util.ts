import { AbstractControl, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";
import * as moment from 'moment';

export class ValidatorUtils {

   /**
     * Metodo que permite validar si el campo es requido.
     * @param msg
     */
    public static required(msg?: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          const res = Validators.required(control);
          if (res) {
            res.message = msg;
            return res;
          }
          return null;
        };
    }

     /**
     * Metodo que permite validar el minimo de caracteres digitados.
     * @param minLength
     * @param msg
     */
    public static minLength(minLength: number, msg?: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          const ref = Validators.minLength(minLength);
          const r = ref(control);
          if (r) {
            r.message = msg;
            return r;
          }
          return null;
        };
    }
    
    /**
     * Metodo que permite validar el máximo de caracteres digitados.
     * @param maxLength
     * @param msg
     */
    public static maxLength(maxLength: number, msg?: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          const ref = Validators.maxLength(maxLength);
          const r = ref(control);
          if (r) {
            r.message = msg;
            return r;
          }
          return null;
        };
    }

    /**
     * Metodo que permite validar estructura del correo eléctronico
     * @param msg
     */
    public static email(msg?: string): ValidatorFn {
      return (control: AbstractControl): ValidationErrors | null => {
        
        if (control.value == undefined || control.value == '' || control.value && this.validateEmail(control.value)) {
          return null;
        } else {
          return {
            email: false,
            message: msg
          };
        }
      };
    }

    /**
     * Metodo que permite validar estructura del correo eléctronico
     * @param msg
     */
    public static validateEmail(email: string) {
      // tslint:disable-next-line:max-line-length
      // tslint:disable-next-line:prefer-const
      var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{2,4})+$/;
      return re.test(email);
    }

    /**
     * Metodo que permite solo texto en los input
     * @param e
     */
    keyPressValTexto(e) {
      if(e.code != 'Tab'){
        var tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        var patron =/[a-zA-ZñÑá-úÁ-Ú\s]/; // 4
        var te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
      }
    }

    /**
     * Metodo que permite solo números en los input
     * @param e
     */
    soloNumeros(e) {
      if(e.code != 'Tab'){
        var tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        var patron =/\d/; // 4
        var te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
      }
    }
    
    /**
     * Valida el n úmero de documento a digitar.
     * Si es cedula solo permite números de lo contrario permite 
     * letras.
     * @param event 
     * @param tipoIdentificacion 
     */
    keyPressValDocumento (event: any, tipoIdentificacion: string) {
      var tecla = (document.all) ? event.keyCode : event.which;
      var patron = /[A-Za-z0-9]/;
      if(event.code != 'Tab' && event.code != 'Backspace'){
        if (  tipoIdentificacion != '62') {
          return this.soloNumeros(event);
        }else{
          var tecla_final = String.fromCharCode(tecla);
          return patron.test(tecla_final);
        }
      }
    }

    /**
     * Filtra la fecha hasta la actual.
     */
    fiterCurrent = (date: Date): boolean => {
      return date  < (new Date()) ;
    }

    public static pattern(pattern: string, msg?: string): ValidatorFn {
      return (control: AbstractControl): ValidationErrors | null => {
        const ref = Validators.pattern(pattern);
        const r = ref(control);
        if (r) {
          r.message = msg;
          return r;
        }
        return null;
      };
    }

    /**
     * Metodo que permite indicar que el campo es obligatorio.
     * @param e
     */
    public getValidationRequired(): { [key: string]: any } {
        return [null, Validators.compose([
            ValidatorUtils.required('Debe diligenciar este campo.')
            ]
        )];
    }

    /**
     * Formatear fecha yyyy-mm-dd
     * @param date 
     */
    formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
  
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
  
      return [day, month, year ].join('/');
  }

  /**
   * Limpia los campos de los formularios.
   * @param controls 
   */
  clearFields(controls: any) {
    let control: AbstractControl = null;
    Object.keys(controls).forEach((name) => {
      control = controls[name];
      control.setErrors(null);
    });
  }

  getValidData(e, date1: Date, date2: Date, msg){
      if(date1 != null && date2 != null){
        if(date1 > date2){
          e.setErrors({ 'incorrect': true, message: msg });
        }
      } 
  }

  public getFechaFin(fechaValidar: Date, edad: number): Date{
    var dateModifi = new Date();
    dateModifi.setDate(new Date(fechaValidar).getDate());
    dateModifi.setMonth(new Date(fechaValidar).getMonth());
    dateModifi.setFullYear(fechaValidar.getFullYear() + edad);
    return dateModifi;
  }

  calcDate(date1, date2): boolean {
    var mayor: boolean = false;
    var years = moment().diff(date1, 'years');

    if (years > 18) {
      mayor = true;
    } else if(years == 18){
      var mes = date2.getMonth() - date1.getMonth();
      if(mes < 0){
        mayor = true;
      } else if(mes == 0){
        var dia = date2.getDate() - date1.getDate();
        if(dia <= 0){
          mayor = true;
        }
      }
    }

    return mayor;
  }
    
}
