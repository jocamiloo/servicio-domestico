import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
//import { AuthenticationService } from '../_services/authentication.service';
import { SERVICE_CONSTANTS } from '../_util/services-util';
import { CONSTANTES_UTIL } from '../_util/constantes-util';
//import { AppConfig } from '../../app/app.config';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

   /* protected baseUrl: string = AppConfig.settings.apiServer.proxyGateway;
    protected clientId: string = AppConfig.settings.oauth2.clientId;
    protected redirectUrl: string = AppConfig.settings.oauth2.redirectUrl; */
    
    constructor(private router: Router /*private authenticationService: AuthenticationService*/) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      /*  // TODO: Cambiar por objeto almacenado el authentication.service
        if (sessionStorage.getItem(CONSTANTES_UTIL.NAME_TOKEN)) {
            // logged in so return true
            if (state.url = '/') {
                return true;
            } else {
                return false;
            }
        }

        const i = window.location.href.indexOf(CONSTANTES_UTIL.CODE);
        if (i !== -1) {
            this.authenticationService.retrieveToken(window.location.href.substring(i + 5));
        } else {
            window.location.href = this.baseUrl + SERVICE_CONSTANTS.REDIRECT_LOGIN +
                this.clientId + CONSTANTES_UTIL.REDIRECT_URI + this.redirectUrl;
        }
        // not logged in so redirect to login page with the return url*/
        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }
}
